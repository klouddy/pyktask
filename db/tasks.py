#!/c/Python33/python

import sys
from pymongo import MongoClient
import datetime
from colorama import init
from colorama import Fore, Back, Style
import hashlib
from textwrap import wrap

init(autoreset=True)


client = MongoClient('localhost', 27017)
db = client.clitasks.tasks

def addTask(task, tags, users, priority):
	date = datetime.datetime.utcnow();
	currTask = {"task": task, "tags": tags, "users": users, "priority": priority, "dtCreated": date}
	currTask['hid'] = getHid(currTask)
	db.insert(currTask )

def printAll():
	printList(db.find())

def printByTag(tag):
	printList(db.find({"tags": { '$in': [tag]}}))


#-------------------------------------------------------------------------
# Handles printing the list of tasks.
# They are passed in tasks from db.tasks.find()
#-------------------------------------------------------------------------

def printList(tasks):
	#print header
	header = {'h1': 'done', 'h2': 'hid', 'h3': 'task','h4': 'pr', 'h5': 'tags'}

	print("\n")
	print(Fore.GREEN + '{h1:5} {h2:5} {h3:50} {h4:4} {h5:10}'.format(**header))
	print(Fore.GREEN + '------------------------------------------------------------------------------')

	table = []
	for task in tasks:
		
		for ln in getTableLine(task):
			table.append(ln)
		

	currhid = ''
	lasthid = ''
	eoo = 0
	for ln in table:
		currhid = ln['h2']
		if currhid != lasthid:
			if eoo == 0:
				eoo = 1
			else:
				eoo = 0
		else:
			ln['h2'] = ''
		lasthid = currhid

		if eoo == 1:
			print(Style.DIM + Back.GREEN + Fore.BLACK + '{h1:5} {h2:5} {h3:50} {h4:4} {h5:10}'.format(**ln))	
		else:
			print('{h1:5} {h2:5} {h3:50} {h4:4} {h5:10}'.format(**ln))
			
	print("\n")

#-------------------------------------------------------------------------
# Get Table line string
#-------------------------------------------------------------------------
def getTableLine(task):
	
	table = []
	numLines = 1
	taskSplit = wrap(task.get('task', ''), 49)
	tags = task.get('tags', None)
	tagsStr = ''
	if(tags != None):
		tagsStr = ', '.join(tags)
	
	tagsSplit = wrap(tagsStr, 9)

	#get lines needed
	if numLines < len(taskSplit):
		numLines = len(taskSplit)
	if numLines < len(tagsSplit):
		numLines = len(tagsSplit)

	if len(taskSplit) == 0:
		taskSplit.append(task.get('task', ''))
	if len(tagsSplit) == 0:
		tagsSplit.append(task.get('tags', ''))
	

	for i in range(0, numLines):
		if i == 0:
			tmp = {'h1': task.get('done', '[ ]'), 'h2': task.get('hid', ''), 'h3': taskSplit[i], 'h4': task.get('priority', '3'), 'h5': tagsSplit[i]}
			table.append(tmp)
		else:
			tmp = {'h1': '', 'h2': task.get('hid',''), 'h3': '', 'h4': '', 'h5': ''}
			
			if(i < len(taskSplit)):
				tmp['h3'] = taskSplit[i]
			if(i < len(tagsSplit)):
				tmp['h5'] = tagsSplit[i]
			table.append(tmp)

	return table	


#-------------------------------------------------------------------------
# Get new hid
#-------------------------------------------------------------------------
def getHid(task):
	m = hashlib.md5()
	m.update(task.get('task', '').encode("utf-8"))
	m.update(str(task.get('date', datetime.datetime.utcnow())).encode("utf-8"))
	hexdi = m.hexdigest()
	hid = hexdi[:4]
	while db.tasks.find({'hid': hid}).count() > 0:
		m.update(hexdi.encode("utf-8"))
		hexdi = m.hexdigest()
		hid = hexdi[:4]
	return hid