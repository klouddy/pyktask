#!/c/Python33/python

from pymongo import MongoClient

client = MongoClient('localhost', 27017)
db = client.clitasks.comments

def createComment(hid, comment):
	return db.insert({"ref": hid, "comment" : comment})