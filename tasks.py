#!/c/Python33/python

import sys
import datetime
from db import tasks

#-------------------------------------------------------------------------
# get task info from tasks add .....
#-------------------------------------------------------------------------
def getTaskCmd(args):
	
	body = ''
	tags = None
	users = None
	priority = '3'

	for i in range(1, len(args) - 1):
		
		if args[i][0:2] == '-t':
			if tags != None:
				tags.append(args[i + 1])
			else:
				tags = [args[i + 1]]
			i = i + 1
		elif args[i][0:2] == '-u':
			if users != None:
				users.append(args[i + 1])
			else:
				users = [args[i + 1]]
			i = i + 1
		elif args[i][0:2] == '-p':
			priority = args[i + 1]
			i = i + 1

		else:
			body = body + ' ' + args[i]

	
	task = {'task': body, 'dtCreated': date, 'tags': tags, 'users': users, 'priority': priority}
	return task



def main(argv):
	

	#first argument is command
	#command options are 
	# - add = add task
	#	task title -t tag -t tag -t tag -u user -u user
	# - list = lists of tasks
	#	all = everything
	#	tag thetag = shows all tasks with thetag tag
	# - check/uncheck = completes task
	#	check tag_hid
	# - archive = archive task
	#	archive tag_hid
	# - comment = comment on task
	#	add task_hid
	#	view task_hid
	# - view task task_hid = shows task with all comments



	if(len(argv) < 1):
		exit()

	command = argv[0]

	if command == "add":
		task = getTaskCmd(argv)
		tasks.addTask(task.get('task', ''), task.get('tags', None), task.get('users', None), task.get('priority', '3'))
			
	elif command == "list":
		#get subcommand assume all
		subcommand = "all"
		if(len(argv) > 1):
			subcommand = argv[1]

		print(subcommand)

		if subcommand == "all":
			tasks.printAll()

		elif subcommand == "tag":
			print(argv[2])
			tasks.printByTag(argv[2])







#-------------------------------------------------------------------------
# This is where it all begins
#-------------------------------------------------------------------------

	
if __name__ == "__main__":
	main(sys.argv[1:])