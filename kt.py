#!/c/Python33/python

import sys
import argparse
from pymongo import MongoClient
import datetime
import ktcomments

from colorama import init
from colorama import Fore, Back, Style
import hashlib
from textwrap import wrap

init(autoreset=True)

client = MongoClient('localhost', 27017)
db = client.clitasks

class PArgs(object):
	pass
	
#-------------------------------------------------------------------------
# Adds task to mongo db tasks table
#-------------------------------------------------------------------------

def addTask(task, tags, users, pr):
	
	date = datetime.datetime.utcnow()
	toAdd = {"task": task, 'done': "[ ]", "tags": tags, "users": users, "priority": pr, "date": date, "completed": 0}
	toAdd['hid'] = getHid(toAdd)
	id = db.tasks.insert(toAdd)

	if len(str(id)) > 0:
		print('Task Added Succesfully')
	

#-------------------------------------------------------------------------
# Get new hid
#-------------------------------------------------------------------------
def getHid(task):
	m = hashlib.md5()
	m.update(task.get('task', '').encode("utf-8"))
	m.update(str(task.get('date', datetime.datetime.utcnow())).encode("utf-8"))
	hexdi = m.hexdigest()
	hid = hexdi[:4]
	while db.tasks.find({'hid': hid}).count() > 0:
		m.update(hexdi.encode("utf-8"))
		hexdi = m.hexdigest()
		hid = hexdi[:4]
	return hid


#-------------------------------------------------------------------------
# updates the hids...
#-------------------------------------------------------------------------

def updateHids():
	tasks = db.tasks.find()
	date = datetime.datetime.utcnow()
	for task in tasks:
		task['hid'] = getHid(task)
		db.tasks.save(task)

#-------------------------------------------------------------------------
# Handles printing the list of tasks.
# They are passed in tasks from db.tasks.find()
#-------------------------------------------------------------------------

def printList(tasks):
	#print header
	header = {'h1': 'done', 'h2': 'hid', 'h3': 'task','h4': 'pr', 'h5': 'tags'}

	print("\n")
	print(Fore.GREEN + '{h1:5} {h2:5} {h3:50} {h4:4} {h5:10}'.format(**header))
	print(Fore.GREEN + '------------------------------------------------------------------------------')

	table = []
	for task in tasks:
		
		for ln in getTableLine(task):
			table.append(ln)
		

	currhid = ''
	lasthid = ''
	eoo = 0
	for ln in table:
		currhid = ln['h2']
		if currhid != lasthid:
			if eoo == 0:
				eoo = 1
			else:
				eoo = 0
		else:
			ln['h2'] = ''
		lasthid = currhid

		if eoo == 1:
			print(Style.DIM + Back.GREEN + Fore.BLACK + '{h1:5} {h2:5} {h3:50} {h4:4} {h5:10}'.format(**ln))	
		else:
			print('{h1:5} {h2:5} {h3:50} {h4:4} {h5:10}'.format(**ln))
			
	print("\n")

#-------------------------------------------------------------------------
# Get Table line string
#-------------------------------------------------------------------------
def getTableLine(task):
	
	table = []
	numLines = 1
	taskSplit = wrap(task.get('task', ''), 49)
	tags = task.get('tags', None)
	tagsStr = ''
	if(tags != None):
		tagsStr = ', '.join(tags)
	
	tagsSplit = wrap(tagsStr, 9)

	#get lines needed
	if numLines < len(taskSplit):
		numLines = len(taskSplit)
	if numLines < len(tagsSplit):
		numLines = len(tagsSplit)

	if len(taskSplit) == 0:
		taskSplit.append(task.get('task', ''))
	if len(tagsSplit) == 0:
		tagsSplit.append(task.get('tags', ''))
	

	for i in range(0, numLines):
		if i == 0:
			tmp = {'h1': task.get('done', '[ ]'), 'h2': task.get('hid', ''), 'h3': taskSplit[i], 'h4': task.get('priority', '3'), 'h5': tagsSplit[i]}
			table.append(tmp)
		else:
			tmp = {'h1': '', 'h2': task.get('hid',''), 'h3': '', 'h4': '', 'h5': ''}
			
			if(i < len(taskSplit)):
				tmp['h3'] = taskSplit[i]
			if(i < len(tagsSplit)):
				tmp['h5'] = tagsSplit[i]
			table.append(tmp)

	return table

#-------------------------------------------------------------------------
# Mark task as completed.
#-------------------------------------------------------------------------
def markCompleted(hid):
	
	task = db.tasks.find({'hid': hid[0]})
	
	if(task.count() == 1):
		t = task[0]
		t['done'] = '[X]'
		t['completed'] = 1
		db.tasks.save(t)

#-------------------------------------------------------------------------
# Un check task
#-------------------------------------------------------------------------
def uncheckTask(hid):
	task = db.tasks.find({'hid': hid[0]})
	if task.count() == 1:
		t = task[0]
		t['done'] = '[ ]'
		t['completed'] = 0
		db.tasks.save(t)


#-------------------------------------------------------------------------
# Main Function -- Run first
#-------------------------------------------------------------------------

def main(argv):
	#parse arguments
	parsedArgs = PArgs()
	parser = argparse.ArgumentParser(description='Tasks and such.')
	parser.add_argument('-l', '--list',action='store_const', const='list')
	parser.add_argument('-bt', '--by_tag', nargs=1)
	parser.add_argument('-a', '--add', nargs='*')
	parser.add_argument('-t', '--tags', nargs='*')
	parser.add_argument('-u', '--users', nargs='*')
	parser.add_argument('-p', '--priority', nargs=1)
	parser.add_argument('--update_hids', action='store_const', const='update')
	parser.add_argument('-c', '--check', nargs=1)
	parser.add_argument('-uc', '--uncheck', nargs=1)
	parser.add_argument('-at', '--add_tag', nargs=2)
	parser.add_argument('--archive', nargs=1)
	parser.add_argument('--view_archived', action='store_const', const='arc')
	parser.add_argument('--comment', nargs="*")
	parser.parse_args(argv, namespace=parsedArgs)

	tags = None
	users = None
	priority = None

	if parsedArgs.tags != None:
		tags = ' '.join(parsedArgs.tags).split(',')
	if parsedArgs.users != None:
		users = ' '.join(parsedArgs.users).split(',')
	if parsedArgs.priority != None:
		priority = parsedArgs.priority
	else:
		priority = '3'

	''' 
	command section
	---------------
	'''

	#list tasks
	if parsedArgs.list == 'list':
		printList(db.tasks.find())

	#list by tag
	if parsedArgs.by_tag != None:
		print(parsedArgs.by_tag[0])
		tasks = db.tasks.find({"tags": { '$in': [parsedArgs.by_tag[0]]}})
		printList(tasks)
		

	#add task
	if parsedArgs.add != None:
		task = ' '.join(parsedArgs.add)
		addTask(task, tags, users, priority)
	
	#update hids command
	if parsedArgs.update_hids == 'update':
		updateHids()

	#check off task
	if parsedArgs.check != None:
		markCompleted(parsedArgs.check)

	#un check task
	if parsedArgs.uncheck != None:
		uncheckTask(parsedArgs.uncheck)

	#archive
	if parsedArgs.archive != None:
		task = db.tasks.find_one({"hid": parsedArgs.archive[0]})
		db.tasks.remove(task)
		
		db.archived_tasks.insert(task)


	if parsedArgs.add_tag != None:
		tasks = db.tasks.find({"hid": parsedArgs.add_tag[0]})
		for t in tasks:
			tags = t.get('tags', None)
			if(tags == None):
				t['tags'] = [parsedArgs.add_tag[1]]
			else:
				t['tags'].append(parsedArgs.add_tag[1])
			db.tasks.save(t)

	#view archived
	if parsedArgs.view_archived == 'arc':
		tasks = db.archived_tasks.find()
		printList(tasks)
	
	#add a comment
	if parsedArgs.comment != None:

		ktcomments.createComment(parsedArgs.comment[0], )
#-------------------------------------------------------------------------
# This is where it all begins
#-------------------------------------------------------------------------

	
if __name__ == "__main__":
	main(sys.argv[1:])